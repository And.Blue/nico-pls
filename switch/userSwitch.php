<?php
require './controller/userControllers.php';

// route /users/x
// x étant la methode à executer : ex: 'all' , 'create'
function userSwitch($param){

    switch ($param[2])
    {
        case 'all':
            showAllUsers();
            break;
        case 'create':
            createUser();
            break;
        default:
            echo 'executed default in userSwitch';

    }
}