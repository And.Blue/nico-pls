<?php
require './model/dbConnect.php';


function getAllUsers()
{
    $db = Db::get();

    $req = $db->query('SELECT * FROM users');
    $users = $req->fetchAll(PDO::FETCH_ASSOC);
    return $users;
}


function repoAddUser($user)
{
    $db = Db::get();
    $req = $db->prepare('INSERT INTO users(name, surname) VALUES (?,?)');
    $req->execute(array($user['name'],$user['surname']));
    $req->closeCursor();
}


