<?php
session_start();
define('PROJECT_PATH', '/fun');

$url = $_SERVER['REQUEST_URI'];
$relativeUrl = str_replace(PROJECT_PATH, '', $url);
$param = explode('/', $relativeUrl);;

switch ($param[1]) {
    case 'login':
        require './controller/authControllers.php';
        login();
        break;
    case 'register':
        require './controller/authControllers.php';
        register();
        break;
    case 'sql':
        require './controller/dupondControllers.php';
        showSql();
        break;

    case 'logout':
        require './controller/authControllers.php';
        logout();
        break;

    case 'rooms':
        require './switch/roomSwitch.php';
        roomSwitch($param);
        break;

    case 'annonces':
        require './controller/testControllers.php';
        test();
        break;

    case 'calculator':
        require './controller/testControllers.php';
        showCalculator();
        break;


    default:
        header('Location: ' . PROJECT_PATH . '/login');
        break;

}