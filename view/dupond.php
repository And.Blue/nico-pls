
<?php
ob_start();
$title = 'SQL';
?>
<h1>Commandes SQL</h1>
<p><em>Récupérer toutes les annonces de l’utilisateur « Dupond » id=6</em></p>
<pre class="prettyprint">
    SELECT * FROM `annonces` WHERE idUtilisateur=6
</pre>

<p>Récupérer Le nombre d’annonces dans la catégorie « Loisirs » idCategory = 1</p>
<pre class="prettyprint">
    SELECT COUNT(*) FROM `classements` WHERE idCategorie=1
</pre>


<?php
$content = ob_get_clean();
require './view/base.php';

