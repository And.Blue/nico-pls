
<?php
ob_start();
$title = 'Calculator';
?>

<div class="container">


<div class="jumbotron" style="margin-top: 2rem">
        <h2 class="display-4">My fun super special calculator</h2>
    </div>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Price Paid</th>
            <th scope="col">Cookie weight</th>
        </tr>
        </thead>
        <tbody id="result">

        </tbody>
    </table>
    <div class="form-group">
    <label for="nameIn">User name</label>
    <input type="text" class="form-control" id="nameIn" placeholder="Enter person name"/>
    <br>

    <label for="priceIn">Price Paid</label>
    <input type="number" class="form-control" id="priceIn" placeholder="price paid in €"/>
    <br>

    <button type="button" class="btn btn-primary" id="addUser" onclick="addUser()">Add User</button>
    <br>
    <label for="weight">Cookie weight in grams</label>
    <input type="number" class="form-control" id="weight" placeholder="number of Gs"/>
    <br>
    <button type="button" class="btn btn-success" onclick="calculate()">Calculate</button>
    </div>
    

    <script>
        const user = document.getElementById('nameIn');
        const price = document.getElementById('priceIn');
        const weight = document.getElementById('weight');
        const resultDiv = document.getElementById('result');

        let form;

        const result = 0;

        const users = [];

        const addUser = () => {
            users.push({
                name: user.value,
                price: price.value,
                weight: null
            });
            console.log(users);
        };

        const calculate = () => {
            console.log(weight.value);
            let totalWeight = weight.value;
            let totalUsers = [];
            let res = weight.value;

            let totalPaid = 0;

            users.map(user => {
                totalPaid += parseInt(user.price);
            });

            users.map(user => {
                totalUsers.push({
                    name: user.name,
                    price: user.price,
                    weight: (totalWeight / parseInt(totalPaid)) * user.price
                })
            });

            console.log(totalUsers);

            totalUsers.map(user => {
                resultDiv.insertAdjacentHTML('beforeend','<tr><td scope="row">'+
                    user.name+'</td><td>'+user.price+'€</td><td>'+user.weight+'g</td></tr>');

            });

        };


    </script>

</div>

<?php
$content = ob_get_clean();
require './view/base.php';
