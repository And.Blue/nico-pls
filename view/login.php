
<?php
ob_start();
$title = 'Login';

?>
<style>
    input[type=password], input[type=text]{
        width: 95%;
    }
</style>

<h1>Login</h1>

<br>
<div class="loginWindow" id="login">
    <form action="" method="post">
        <label for="email"><br />Email</label><br />
        <input id="email" type="text" name="email" placeholder="Ex : Toto@example.com"><br /><br>
        <label for="password">Mot-de-Passe</label><br />
        <input id="password" type="password" name="password" placeholder="Mot-de-Passe" size="30"><br /><br>

        <button class="" type="submit" style="vertical-align:middle; color: white"><span>Login</span></button>
    </form>
</div>
<p>
    Pas encore inscrit ? <a href="<?= PROJECT_PATH ?>/register"><em>Cliquez ici</em></a>
</p>

<?php
$content = ob_get_clean();
require './view/base.php';