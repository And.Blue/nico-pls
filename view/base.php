<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--    FONTS-->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!--    ICONS
    https://fontawesome.com/v4.7.0/examples/
    -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>
    <!--    CSS-->    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= PROJECT_PATH?>/css/style1.css">
    <title>TP NOTE</title>

</head>
<body>


<!--NAVBAR-->
<?php

    require 'navbar/navbar1.php';

    ?>

    <!--CONTENT-->
    <div class="container">
        <?php
        if(isset($notification)) {
            echo $notification;
        }
        echo $content;
        ?>
    </div>
<?php

?>
</body>
</html>
