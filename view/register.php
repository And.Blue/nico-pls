<?php
ob_start();
?>
<div class="row">
    <div class="col-6 col-s-9">
        <h1>S'inscrire</h1>
        <p>Veuillez mettre ci-dessous les informations d'inscription</p>
        <form method="post">
            <input type="text" placeholder="Nom d'Utilisateur" name="email" onkeyup="callFunction(this)">
            <div style="">
                <div id="info"></div>
                <input type="password" style="width: 500px" placeholder="Entrez votre Mot-de-Passe" name="pw" id="pass1" >

            </div>

            <input type="password" placeholder="Confirmation du Mot-de-Passe" name="pw-confirm" id="pass2" onkeyup="checkPw()">
            <input type="submit" value="S'inscrire">
        </form>
    </div>

</div>

<script>
    const res = document.getElementById('info');
    const pass1 = document.getElementById('pass1');
    const pass2 = document.getElementById('pass2');

    const checkPw = () => {
        if(pass1.value !== pass2.value) {
            res.innerHTML = '<label style="color: red">Les Mots-de-Passe ne sont pas les mêmes Nok</label>'
        } else {
            res.innerHTML = '<label style="color: green">Les mots de passe sont bons! ok </label>'
        }
    };

    const callFunction = (args) => {
        const textBox = document.getElementById("pass1");
        res.innerText ='';

        if (args.value == '' || args.value.length < 8) {
            res.innerText ='Login trop court';
        }
    }
</script>

<?php
$content = ob_get_clean();
require './view/base.php';

